import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.url)

WebUI.waitForElementPresent(findTestObject('Login Page/TextTitle'), 0)

WebUI.verifyElementPresent(findTestObject('Login Page/TextTitle'), 0)

WebUI.verifyElementClickable(findTestObject('Login Page/btnMakeAppointment'))

WebUI.click(findTestObject('Login Page/btnMakeAppointment'))

WebUI.waitForElementVisible(findTestObject('Login Page/txtfieldPassword'), 0)

usernameDemo = WebUI.getAttribute(findTestObject('Login Page/txtUsernameDemo'), 'value')

passwordDemo = WebUI.getAttribute(findTestObject('Login Page/txtPasswordDemo'), 'value')

WebUI.setText(findTestObject('Login Page/txtfieldUsername'), usernameDemo)

WebUI.setText(findTestObject('Login Page/txtfieldPassword'), passwordDemo)

WebUI.click(findTestObject('Login Page/btnLogin'))

WebUI.verifyElementPresent(findTestObject('Make Appointment Page/textTitleMakeAppointment'), 0)

WebUI.click(findTestObject('Make Appointment Page/btnMenuHamburger'))

WebUI.waitForElementVisible(findTestObject('Make Appointment Page/btnLogout'), 0)

WebUI.click(findTestObject('Make Appointment Page/btnLogout'))

WebUI.waitForElementVisible(findTestObject('Login Page/TextTitle'), 0)

WebUI.verifyTextPresent('CURA Healthcare Service', true)

WebUI.verifyTextPresent('We Care About Your Health', true)

WebUI.closeBrowser()

