<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txtTitleAppointmentConfirmation</name>
   <tag></tag>
   <elementGuidId>835d7da3-b28b-4e9d-9060-e5fae4cff731</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div/h2[text()=&quot;Appointment Confirmation&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
