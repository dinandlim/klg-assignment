<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txtLoginErrorMessage</name>
   <tag></tag>
   <elementGuidId>5c0f2871-d984-464c-bf47-ffd193840d0f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div/p[text()=&quot;Login failed! Please ensure the username and password are valid.&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
